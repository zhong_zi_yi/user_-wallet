/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100516
 Source Host           : localhost:3306
 Source Schema         : user_transfer_money

 Target Server Type    : MySQL
 Target Server Version : 100516
 File Encoding         : 65001

 Date: 19/11/2022 19:50:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_account
-- ----------------------------
DROP TABLE IF EXISTS `t_account`;
CREATE TABLE `t_account`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `money` decimal(12, 2) NULL DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '金额表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_account
-- ----------------------------
INSERT INTO `t_account` VALUES ('3b5738b0-4885-4fb6-92e1-dab300680984', 'c6234661-e84f-451f-8d1f-d0bae4d16a10', 300.00);
INSERT INTO `t_account` VALUES ('3cb76ef2-5746-44d5-bbd5-34a76d20cfbb', '630adab2-363b-4d69-ac57-dd64ea82682d', 300.00);

-- ----------------------------
-- Table structure for t_log
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `transfer_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转账用户id',
  `proceeds_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收款用户id',
  `money` decimal(12, 2) NULL DEFAULT NULL COMMENT '操作金额',
  `operation_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类型',
  `operation_state` int NULL DEFAULT NULL COMMENT '操作状态(1 成功/0 失败)',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_log
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `user_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `account_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户名称',
  `state` int NULL DEFAULT 1 COMMENT '状态',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('630adab2-363b-4d69-ac57-dd64ea82682d', 'Admin', '$2a$10$yfcKRMlzThlzfm/7KWHTp.tGTCIFLHslmTDmGa0Nu1LIdSibxjujK', '17770914454', '630adab2-363b-4d69-ac57-dd64ea82682dAdmin', 1, '2022-11-19 01:35:32');
INSERT INTO `t_user` VALUES ('c6234661-e84f-451f-8d1f-d0bae4d16a10', '钟子义', '$2a$10$6Ya1kAWE9aubcYokEdmCHuWpCX.viWPI68sPTmaWfxBIDv/bNGQmi', '17770914453', 'c6234661-e84f-451f-8d1f-d0bae4d16a10钟子义', 1, '2022-11-19 01:35:32');

SET FOREIGN_KEY_CHECKS = 1;
