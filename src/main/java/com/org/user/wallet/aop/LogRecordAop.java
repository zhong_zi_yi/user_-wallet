package com.org.user.wallet.aop;

import com.org.user.wallet.pojo.dto.UpdateMoneyDTO;
import com.org.user.wallet.pojo.entity.Log;
import com.org.user.wallet.pojo.vo.LogVO;
import com.org.user.wallet.pojo.vo.UserVO;
import com.org.user.wallet.service.LogService;
import com.org.user.wallet.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.UUID;

/**
 * 日志记录
 */
@Slf4j
@Aspect
@Component
public class LogRecordAop {

    @Autowired
    LogService logService;

    @Autowired
    UserService userService;

    UpdateMoneyDTO updateMoneyDTO = null;
    LogVO logVO = null;
    UserVO ProceedsUser = null;
    UserVO TransferUser = null;

    @Pointcut("execution(public * com.org.user.wallet.service.AccountService.updateMoneyByUserId(..))")
    public void updateMoneyByUserId() {
    }

    @Pointcut("execution(public * com.org.user.wallet.service.AccountService.updateMoneyByUserIdRefund(..))")
    public void updateMoneyByUserIdRefund() {
    }

    //用户消费扣款成功记录日志
    @AfterReturning("updateMoneyByUserId()")
    public void beforeUpdateMoneyLogRecord(JoinPoint joinPoint) {
        log.info("执行后");
        updateMoneyDTO = (UpdateMoneyDTO) Arrays.asList(joinPoint.getArgs()).get(0);
        String info = "用户【" + updateMoneyDTO.getTransferUserName() + "】向【" + updateMoneyDTO.getProceedsUserName()
                + "】消费【" + updateMoneyDTO.getMoney() + "】金额";
        //获取转账用户id
        ProceedsUser = userService.getUserByUserName(updateMoneyDTO.getProceedsUserName());
        //收款用户id
        TransferUser = userService.getUserByUserName(updateMoneyDTO.getTransferUserName());
        saveSysLog(info, updateMoneyDTO.getMoney(), "消费", 1);
    }
    //用户消费扣款失败记录日志
    @AfterThrowing(value = "updateMoneyByUserId()", throwing = "t")
    public void throwingUpdateMoneyLogRecord(JoinPoint joinPoint, Throwable t) {
        log.info("异常时！");
        updateMoneyDTO = (UpdateMoneyDTO) Arrays.asList(joinPoint.getArgs()).get(0);
        String info = "用户【" + updateMoneyDTO.getTransferUserName() + "】向【" + updateMoneyDTO.getProceedsUserName()
                + "】消费【" + updateMoneyDTO.getMoney() + "】金额时发生错误！错误信息：【" + t + "】";
        //获取转账用户id
        ProceedsUser = userService.getUserByUserName(updateMoneyDTO.getProceedsUserName());
        //收款用户id
        TransferUser = userService.getUserByUserName(updateMoneyDTO.getTransferUserName());
        saveSysLog(info, updateMoneyDTO.getMoney(), "消费", 0);
    }


    //退款成功记录日志
    @AfterReturning("updateMoneyByUserIdRefund()")
    public void beforeUpdateMoneyByUserIdRefund(JoinPoint joinPoint) {
        log.info("执行后");
        logVO = logService.getLogById(Arrays.asList(joinPoint.getArgs()).get(0).toString());
        TransferUser = userService.getUserById(logVO.getProceedsUserId());
        ProceedsUser = userService.getUserById(logVO.getTransferUserId());
        String info = "用户【" + TransferUser.getUserName() + "】向【" + ProceedsUser.getUserName()
                + "】退款【" + logVO.getMoney() + "】金额";
        saveSysLog(info, logVO.getMoney(), "退款", 1);
    }
    //退款失败记录日志
    @AfterThrowing(value = "updateMoneyByUserIdRefund()", throwing = "t")
    public void throwingUpdateMoneyByUserIdRefund(JoinPoint joinPoint, Throwable t) {
        log.info("异常时！");
        String id = Arrays.asList(joinPoint.getArgs()).get(0).toString();
        logVO = logService.getLogById(id);
        log.info(">>>>>>>>>>>{}", logVO);
        TransferUser = userService.getUserById(logVO.getProceedsUserId());
        ProceedsUser = userService.getUserById(logVO.getTransferUserId());
        String info = "用户【" + TransferUser.getUserName() + "】向【" + ProceedsUser.getUserName()
                + "】退款【" + logVO.getMoney() + "】金额时发生错误！错误信息：【" + t + "】";
        saveSysLog(info, logVO.getMoney(), "退款", 0);
    }


    //记录日志
    public void saveSysLog(String info, BigDecimal money, String operationType, int operationState) {
        Log logObject = new Log();
        logObject.setId(UUID.randomUUID().toString());
        logObject.setInfo(info);
        logObject.setMoney(money);
        logObject.setOperationType(operationType);
        logObject.setOperationState(operationState);
        logObject.setTransferUserId(TransferUser.getId());
        logObject.setProceedsUserId(ProceedsUser.getId());
        System.out.println(logObject.toString());
        logService.insertLog(logObject);
    }
}
