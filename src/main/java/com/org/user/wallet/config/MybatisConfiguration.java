package com.org.user.wallet.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis配置类
 */
@Configuration
@MapperScan("com.org.user.wallet.mapper")
public class MybatisConfiguration {
}
