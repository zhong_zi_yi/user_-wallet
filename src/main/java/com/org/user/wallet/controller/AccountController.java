package com.org.user.wallet.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.org.user.wallet.pojo.dto.UpdateMoneyDTO;
import com.org.user.wallet.pojo.dto.UserDTO;
import com.org.user.wallet.pojo.vo.AccountVO;
import com.org.user.wallet.pojo.vo.UserAccountVO;
import com.org.user.wallet.service.AccountService;
import com.org.user.wallet.web.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  用户钱包管理 Controller 控制器
 */
@Api(tags = "1、用户钱包管理模块")
@Slf4j
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * 查询余额
     * @param userDTO
     * @return
     */
    //localhost:8080/account/login
    @ApiOperation("查询用户钱包余额")
    @ApiOperationSupport(order = 100)
    @PostMapping("/login")
    public JsonResult getAccountByUsername(@RequestBody UserDTO userDTO){
        log.info("开始处理查询余额");
        UserAccountVO Account = accountService.getByAccountByUserName(userDTO);
        return JsonResult.ok(Account);
    }

    /**
     * 消费
     * @param updateMoneyDTO
     * @return
     */
    //localhost:8080/account/consumption
    @ApiOperation("用户消费")
    @ApiOperationSupport(order = 200)
    @PostMapping("/consumption")
    public JsonResult updateMoneyByUserId(@RequestBody UpdateMoneyDTO updateMoneyDTO){
        accountService.updateMoneyByUserId(updateMoneyDTO);
        return JsonResult.ok();
    }

    @ApiOperation("根据日志id退款")
    @ApiOperationSupport(order = 300)
    @ApiModelProperty(value = "日志id",notes = "日志id",access = "日志id")
    @PostMapping("/Refund")
    public JsonResult updateMoneyByUserIdRefund(@RequestParam("id") String id){
        accountService.updateMoneyByUserIdRefund(id);
        return JsonResult.ok();
    }
}
