package com.org.user.wallet.ex.handler;

import com.org.user.wallet.ex.ServiceException;
import com.org.user.wallet.web.JsonResult;
import com.org.user.wallet.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一处理异常
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    public GlobalExceptionHandler() {
        log.debug("创建统一处理异常的对象：GlobalExceptionHandler");
    }

    @ExceptionHandler
    public JsonResult handleServiceException(ServiceException e) {
        log.error("统一处理ServiceException，将向客户端响应：{}", e.getMessage());
        return JsonResult.fail(e);
    }

    @ExceptionHandler
    public JsonResult handleThrowable(Throwable e) {
        log.error("统一处理未明确处理的异常【{}】，将向客户端响应：{}", e.getClass().getName(), e.getMessage());
        e.printStackTrace();
        String message = "服务器忙，请联系管理员！";
        return JsonResult.fail(ServiceCode.ERR_UNKNOWN, message);
    }

}
