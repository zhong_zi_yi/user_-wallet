package com.org.user.wallet.mapper;

import com.org.user.wallet.pojo.entity.Account;
import com.org.user.wallet.pojo.vo.UserAccountVO;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * 钱包余额管理 Mapper 接口
 */
@Repository
public interface AccountMapper {

    /**
     * 添加钱包
     * @param account
     * @return
     */
//    int insertAccount(Account account);

    /**
     *根据用户Id查询钱包余额
     * @param userId
     * @return
     */
    UserAccountVO getAccountByUserId(String userId);

    /**
     * 根据用户id修改金额
     * @return
     */
    int updateMoneyByUserId(String userId, BigDecimal money);

}
