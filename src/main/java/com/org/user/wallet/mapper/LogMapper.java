package com.org.user.wallet.mapper;

import com.org.user.wallet.pojo.entity.Log;
import com.org.user.wallet.pojo.vo.LogVO;
import org.springframework.stereotype.Repository;

/**
 * 日志管理 Mapper 接口
 */
@Repository
public interface LogMapper {
    /**
     * 添加日志信息
     * @param logObject
     * @return
     */
    int insertLog(Log logObject);

    /**
     * 根据id查询日志信息
     * @param id
     * @return
     */
    LogVO getLogById(String id);
}
