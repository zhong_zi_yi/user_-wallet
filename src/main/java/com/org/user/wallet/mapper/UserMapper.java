package com.org.user.wallet.mapper;

import com.org.user.wallet.pojo.entity.User;
import com.org.user.wallet.pojo.vo.UserVO;
import org.springframework.stereotype.Repository;

/**
 * 用户管理 Mapper 接口
 */
@Repository
public interface UserMapper {

    /**
     * 添加用户
     * @param user
     * @return
     */
//    int insertUser(User user);

    /**
     *根据用户名称查询用户钱包余额
     * @param userName
     * @return
     */
    UserVO getUserByUserName(String userName);

    /**
     *根据用户Id查询用户
     * @param id
     * @return
     */
    UserVO getUserById(String id);
}
