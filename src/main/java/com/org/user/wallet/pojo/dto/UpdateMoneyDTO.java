package com.org.user.wallet.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用于做消费功能
 */
import java.math.BigDecimal;

@Data
public class UpdateMoneyDTO {
    /**
     * 转账用户名称
     */
    @ApiModelProperty("转账用户名称")
    private String transferUserName;

    /**
     * 收款用户名称
     */
    @ApiModelProperty("收款用户名称")
    private String proceedsUserName;

    /**
     * 金额
     */
    @ApiModelProperty("金额")
    private BigDecimal money;
}
