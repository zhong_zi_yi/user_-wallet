package com.org.user.wallet.pojo.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Account {
    /**
     * id
     */
    public String id;
    /**
     * 用户id
     */
    public String userId;
    /**
     * 金额
     */
    public BigDecimal money;
}
