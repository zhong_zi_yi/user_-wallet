package com.org.user.wallet.pojo.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Log {
    /**
     * id
     */
    public String id;
    /**
     * 日志信息
     */
    public String info;
    /**
     * 转账用户id
     */
    public String transferUserId;
    /**
     * 收款用户id
     */
    public String proceedsUserId;
    /**
     * 操作金额
     */
    public BigDecimal money;
    /**
     * 造作类型
     */
    public String operationType;
    /**
     * 操作状态(1 true/0 false)
     */
    public int operationState;
    /**
     * 创建日期
     */
    public String createDate;
}
