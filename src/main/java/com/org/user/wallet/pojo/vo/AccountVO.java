package com.org.user.wallet.pojo.vo;

import lombok.Data;
import org.springframework.util.DigestUtils;

import java.math.BigDecimal;

@Data
public class AccountVO {
    /**
     * id
     */
    public String id;

    /**
     * 用户id
     */
    public String userId;

    /**
     * 金额
     */
    public BigDecimal money;
}
