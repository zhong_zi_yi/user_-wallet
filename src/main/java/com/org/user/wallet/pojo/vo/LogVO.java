package com.org.user.wallet.pojo.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LogVO {
    /**
     * 转账用户id
     */
    public String transferUserId;

    /**
     * 收款用户id
     */
    public String proceedsUserId;

    /**
     * 操作金额
     */
    public BigDecimal money;

    /**
     * 操作状态(1 true/0 false)
     */
    public int operationState;
}
