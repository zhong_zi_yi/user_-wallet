package com.org.user.wallet.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户信息及金额
 */
@Data
public class UserAccountVO {
    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String userName;

    /**
     * 账户名称
     */
    @ApiModelProperty("账户名称")
    private String accountName;

    /**
     * 金额
     */
    @ApiModelProperty("金额")
    public BigDecimal money;
}
