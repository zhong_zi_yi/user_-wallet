package com.org.user.wallet.pojo.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class UserVO {
    /**
     * id
     */
    private String id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 账户名称
     */
    private String accountName;

    /**
     * 状态
     */
    private int state;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 金额
     */
    public BigDecimal money;
}
