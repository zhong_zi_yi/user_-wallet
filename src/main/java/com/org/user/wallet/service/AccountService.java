package com.org.user.wallet.service;

import com.org.user.wallet.pojo.dto.UpdateMoneyDTO;
import com.org.user.wallet.pojo.dto.UserDTO;
import com.org.user.wallet.pojo.vo.LogVO;
import com.org.user.wallet.pojo.vo.UserAccountVO;

import java.math.BigDecimal;

/**
 * 钱包余额管理 Service 接口
 */
public interface AccountService {

    /**
     * 根据用户名称查询用户钱包余额
     * @param userDTO
     * @return
     */
    UserAccountVO getByAccountByUserName(UserDTO userDTO);

    /**
     * 根据用户id修改金额
     * @return
     */
    void updateMoneyByUserId(UpdateMoneyDTO transferMoneyDTO);

    /**
     * 根据用户id修改金额（退款）
     * @param id
     */
    void updateMoneyByUserIdRefund(String id);
}
