package com.org.user.wallet.service;

import com.org.user.wallet.pojo.entity.Log;
import com.org.user.wallet.pojo.vo.LogVO;

public interface LogService {
    /**
     * 添加日志信息
     * @param logObject
     * @return
     */
    void insertLog(Log logObject);

    /**
     * 根据id查询日志信息
     * @param id
     * @return
     */
    LogVO getLogById(String id);
}
