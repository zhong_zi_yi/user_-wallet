package com.org.user.wallet.service;

import com.org.user.wallet.pojo.vo.UserVO;

/**
 * 用户管理 Server 接口
 */
public interface UserService {

    /**
     * 根据用户名称查询用户信息
     * @param userName
     * @return
     */
    UserVO getUserByUserName(String userName);

    /**
     *根据用户Id查询用户
     * @param id
     * @return
     */
    UserVO getUserById(String id);
}
