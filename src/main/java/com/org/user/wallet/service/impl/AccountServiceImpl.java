package com.org.user.wallet.service.impl;

import com.org.user.wallet.ex.ServiceException;
import com.org.user.wallet.mapper.AccountMapper;
import com.org.user.wallet.mapper.LogMapper;
import com.org.user.wallet.pojo.dto.UpdateMoneyDTO;
import com.org.user.wallet.pojo.dto.UserDTO;
import com.org.user.wallet.pojo.vo.LogVO;
import com.org.user.wallet.pojo.vo.UserAccountVO;
import com.org.user.wallet.pojo.vo.UserVO;
import com.org.user.wallet.service.AccountService;
import com.org.user.wallet.service.UserService;
import com.org.user.wallet.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    UserService userService;

    @Autowired
    LogMapper logMapper;

    /**
     * 查询余额
     * @param userDTO
     * @return
     */
    @Override
    public UserAccountVO getByAccountByUserName(UserDTO userDTO) {
        log.info("开始处理查询余额业务，参数：{}",userDTO);
        //判断用户名和密码是否为空
        if ("".equals(userDTO.getUserName()) || "".equals(userDTO.getPassword())){
            String message = "用户名以及密码都不能为空！";
            throw new ServiceException(ServiceCode.ERR_NOT_FOUND, message);
        }

        UserVO user = userService.getUserByUserName(userDTO.getUserName());
        log.info("user：{}", user);
        //判断用户名是否存在，为null则是不存在
        if (user == null){
            String message = "该用户不存在！";
            throw new ServiceException(ServiceCode.ERR_NAME_PASSWORD, message);
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        //进行密码的验证
        boolean matches = passwordEncoder.matches(userDTO.getPassword(), user.getPassword());
        if (matches != true){
            String message = "密码错误请重新输入！";
            throw new ServiceException(ServiceCode.ERR_NAME_PASSWORD, message);
        }
        UserAccountVO account = accountMapper.getAccountByUserId(user.getId());
        log.info("AccountVO：{}", account);
        return account;
    }

    /**
     * 消费
     * @param transferMoneyDTO
     */
    @Transactional
    @Override
    public void updateMoneyByUserId(UpdateMoneyDTO transferMoneyDTO) {
        log.info("开始处理消费100元业务，参数：{}",transferMoneyDTO);
        //获取用户并判断用户名是否存在，为null则是不存在
        UserVO TransferUser = userService.getUserByUserName(transferMoneyDTO.getTransferUserName());
        if (TransferUser == null){
            String message = "【" + transferMoneyDTO.getTransferUserName() +"】用户不存在！";
            throw new ServiceException(ServiceCode.ERR_NAME_PASSWORD, message);
        }
        UserVO ProceedsUser = userService.getUserByUserName(transferMoneyDTO.getProceedsUserName());
        if (ProceedsUser == null){
            String message = "【" + transferMoneyDTO.getProceedsUserName() +"】用户不存在！";
            throw new ServiceException(ServiceCode.ERR_NAME_PASSWORD, message);
        }
        /*
            【注意】
            TODO 正常情况下，这里还需要进行密码校验
         */

        //获取消费用户金额
        UserAccountVO TransferAccount = accountMapper.getAccountByUserId(TransferUser.getId());
        BigDecimal TransferMoney = new BigDecimal(TransferAccount.getMoney().toString());
        //收款者用户金额
        UserAccountVO ProceedsAccount = accountMapper.getAccountByUserId(ProceedsUser.getId());
        BigDecimal ProceedsMoney = new BigDecimal(ProceedsAccount.getMoney().toString());

        //判断用户中余额是否够，-1 代表money 小于 transferMoneyDTO.getMoney()
        //【注意】0代表等于  1代表大于  > -1代表大于等于  < 1代表小于等于
        if (TransferMoney.compareTo(transferMoneyDTO.getMoney()) == -1){
            String message = "金额不足，请及时充值！";
            throw new ServiceException(ServiceCode.ERR_UPDATE, message);
        }
        //消费扣款
        accountMapper.updateMoneyByUserId(TransferAccount.getId(), TransferMoney.subtract(transferMoneyDTO.getMoney()));
        //用户收款
        accountMapper.updateMoneyByUserId(ProceedsAccount.getId(), ProceedsMoney.add(transferMoneyDTO.getMoney()));
//        int a = 1/0;
    }

    /**
     * 退款
     * @param id
     */
    @Transactional
    @Override
    public void updateMoneyByUserIdRefund(String id) {
        LogVO logVo = logMapper.getLogById(id);
        if (logVo.getOperationState() == 0){
            String message = "当前日志不可操作，请操作状态为【成功/1】的日志";
            throw new ServiceException(ServiceCode.ERR_UPDATE, message);
        }
        //收款方金额
        UserAccountVO TransferAccount = accountMapper.getAccountByUserId(logVo.getTransferUserId());
        BigDecimal TransferMoney = new BigDecimal(TransferAccount.getMoney().toString());
        //退款方金额
        UserAccountVO ProceedsAccount = accountMapper.getAccountByUserId(logVo.getProceedsUserId());
        BigDecimal ProceedsMoney = new BigDecimal(ProceedsAccount.getMoney().toString());

        //判断用户中余额是否够，-1 代表money 小于 transferMoneyDTO.getMoney()
        if (ProceedsMoney.compareTo(logVo.getMoney()) == -1){
            String message = "退款失败! 由于【" + ProceedsAccount.getUserName() + "】金额不足!";
            throw new ServiceException(ServiceCode.ERR_UPDATE, message);
        }
        //执行退款
        //收款者扣款
        accountMapper.updateMoneyByUserId(TransferAccount.getId(), TransferMoney.add(logVo.getMoney()));
        //消费者收款
        accountMapper.updateMoneyByUserId(ProceedsAccount.getId(), ProceedsMoney.subtract(logVo.getMoney()));
    }


}
