package com.org.user.wallet.service.impl;

import com.org.user.wallet.mapper.LogMapper;
import com.org.user.wallet.mapper.UserMapper;
import com.org.user.wallet.pojo.entity.Log;
import com.org.user.wallet.pojo.vo.LogVO;
import com.org.user.wallet.pojo.vo.UserVO;
import com.org.user.wallet.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    LogMapper logMapper;

    @Autowired
    UserMapper userMapper;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void insertLog(Log logObject) {
        logMapper.insertLog(logObject);
    }

    @Override
    public LogVO getLogById(String id) {
        LogVO logVO = logMapper.getLogById(id);
        return logVO;
    }
}
