package com.org.user.wallet.service.impl;

import com.org.user.wallet.mapper.UserMapper;
import com.org.user.wallet.pojo.vo.UserVO;
import com.org.user.wallet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserVO getUserByUserName(String userName) {
        UserVO user = userMapper.getUserByUserName(userName);
        return user;
    }

    @Override
    public UserVO getUserById(String id) {
        UserVO userVO = userMapper.getUserById(id);
        return userVO;
    }
}
