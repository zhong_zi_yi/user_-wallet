package com.org.user.wallet.web;

import com.org.user.wallet.ex.ServiceException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用于封装服务器端向客户端响应结果的类型
 */
@Data
public final class JsonResult {

    /**
     * 业务状态码
     */
    @ApiModelProperty("业务状态码")
    private Integer code;
    /**
     * 错误时的信息
     */
    @ApiModelProperty("错误信息")
    private String message;
    /**
     * 处理成功时，需要响应到客户端的数据
     */
    @ApiModelProperty("数据")
    private Object data;

    public static JsonResult ok() {
        return ok(null);
    }

    public static JsonResult ok(Object data) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.code = ServiceCode.OK;
        jsonResult.data = data;
        return jsonResult;
    }

    public static JsonResult fail(ServiceException e) {
        return fail(e.getServiceCode(), e.getMessage());
    }

    public static JsonResult fail(Integer code, String message) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.code = code;
        jsonResult.message = message;
        return jsonResult;
    }

}
