package com.org.user.wallet.web;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 业务状态码
 */
public final class ServiceCode {

    /**
     * 成功
     */
    @ApiModelProperty("成功")
    public static final int OK = 20000;
    /**
     * 错误：数据不存在
     */
    @ApiModelProperty("数据不存在")
    public static final int ERR_NOT_FOUND = 40400;

    /**
     * 错误：用户名或密码不正确
     */
    public static final int ERR_NAME_PASSWORD = 40100;
    /**
     * 错误：插入数据失败
     */
    public static final int ERR_INSERT = 50000;
    /**
     * 错误：删除数据失败
     */
    public static final int ERR_DELETE = 50001;
    /**
     * 错误：更新数据失败
     */
    public static final int ERR_UPDATE = 50002;
    /**
     * 错误：未处理的异常
     */
    public static final int ERR_UNKNOWN = 59999;

}
