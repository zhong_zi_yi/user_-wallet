package com.org.user.wallet;

import com.org.user.wallet.mapper.AccountMapper;
import com.org.user.wallet.mapper.UserMapper;
import com.org.user.wallet.pojo.entity.Account;
import com.org.user.wallet.pojo.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.UUID;

@SpringBootTest
public class UserTest {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    public void testAddUser(){
        //密码
        String password = "123456";
        //加密
        password = passwordEncoder.encode(password);
        //生成UUID
        String id = UUID.randomUUID().toString();
        User user = new User();
        user.setId(id);
        user.setUserName("钟子义");
        user.setPassword(password);
        user.setAccountName(user.getId()+user.getUserName());
        System.out.println(user.toString());

        Account account = new Account();
        account.setId(UUID.randomUUID().toString());
        account.setUserId(id);
        account.setMoney(BigDecimal.valueOf(500));
        System.out.println(account.toString());
    }
}
